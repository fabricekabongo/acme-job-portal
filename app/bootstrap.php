<?php
/*
 * This is the bootstrap, I use it to set up the app, load services and controllers. 
 * I'm not going to do it in a OOP manner as it's not neccessary for this assessement.  
 */
require_once __DIR__.'/../vendor/autoload.php';

use Silex\Application;

define('CONFIGURATION_FILE_PATH', __DIR__.'/config.json');

$app = new Application();

//load the configuration
if (!is_file(CONFIGURATION_FILE_PATH)) {
    throw new \Exception(sprintf("Configuration file not found at %s.", CONFIGURATION_FILE_PATH));
}
$app['config'] = json_decode(file_get_contents(CONFIGURATION_FILE_PATH), true);
$app['debug'] = $app['config']['application']['debug'];

?>