<?php

use Acme\StatisticController;

$app['statistic.controller'] = function () use ($app) {
  return new StatisticController($app['statistic.builder'], $app['twig'], $app['monolog']);
};

$a = $app['statistic.controller'];

$app->get('/weekly/data.json', "statistic.controller:jsonWeeklyDataAction");
$app->get('/weekly/data.csv', "statistic.controller:csvWeeklyDataAction");
?>