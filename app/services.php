<?php

use Acme\StatisticBuilder;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\SerializerServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;

$app->register(new ServiceControllerServiceProvider());
$app->register(new SerializerServiceProvider());
$app->register(new TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views',
    'twig.options' => array(
        'cache' => __DIR__.'/cache'
    )
));
$now = new \DateTime();
$app->register(new Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__.'/logs/log-'. $now->format('y-m-d') .'.log',
));
$app->register(new HttpFragmentServiceProvider());

$app['soap.client'] = function () use ($app) {
    $soapArguments = array(
        'exceptions' => $app['config']['soap']['exceptions'],
        'trace' => $app['config']['soap']['trace'],
        'cache_wsdl' => WSDL_CACHE_NONE,
        'soap_version' => SOAP_1_1
    );
    // create SOAP Client
    $client = new SoapClient($app['config']['soap']['wsdl'], $soapArguments);
    // Authenticate with username and password
    $sessionId = $client->login($app['config']['soap']['username'], $app['config']['soap']['password']);
    
    $app['soap.session'] = $sessionId;
    return $client;
};

$app['statistic.builder'] = function () use ($app) {
  return new StatisticBuilder($app['soap.client'], $app['soap.session']);
};

?>