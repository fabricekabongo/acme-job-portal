<?php
require __DIR__. '/../app/bootstrap.php';
require __DIR__. '/../app/services.php';
require __DIR__. '/../app/controllers.php';

//main view. the system will run as the single page app
$app->get('/', function () use($app) {
    return $app['twig']->render('index.html.twig');
});

$app->run();
?>