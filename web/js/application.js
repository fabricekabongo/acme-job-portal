var app = angular.module('statistic', ['ngRoute', 'chart.js'])

    .config(function ($routeProvider) {

        $routeProvider
            .when('/table', {
                controller: 'TableController',
                templateUrl: '/js/view/table.html'
            })
            .when('/bargraph', {
                controller: 'BarController',
                templateUrl: '/js/view/bar.html'
            })
            .otherwise({
                redirectTo: '/table'
            });
    })

    .service('StatisticRepository', function ($http, $q) {
        return {
            'fetchData': function (initialDate) {
                var parameter = '';
                if (typeof initialDate != 'undefined') {
                    parameter += "?start_date=" + initialDate.getFullYear() + '-' + (initialDate.getMonth()+1) + '-' + initialDate.getDate();
                }
                return $http({
                    'url': '/weekly/data.json' + parameter,
                    'method': 'GET'
                }).then(function (response) {
                    if (response.data.status == "success") {
                        return response.data.data;
                    }
                    console.error(response.data);

                    return $q.reject(response.data);
                }, function (error) {
                    console.error(error);

                    return error;
                });
            }
        }
    })

    .controller('TableController', function ($scope, StatisticRepository) {
        $scope.dateStart = null;
        $scope.loading = false;
        
        $scope.getRegion = function (index) {
            return this.regions[index];  
        };
        
        var onDataReceived = function (data) {
            $scope.regions = Object.keys(data.statistic);
            $scope.weeks = data.weeks;
            $scope.statistic = data.statistic;
        };
        
        var onError = function (error) {
            alert('We had an issue loading the data. Please reload page');
        };
        
        var onFinally = function () {
            $scope.loading = false;
        }
        
        $scope.updateStatistic = function () {
            $scope.loading = true;
            StatisticRepository.fetchData(this.dateStart)
                .then(onDataReceived, onError)
                .finally(onFinally);
        };
        
        $scope.loading = true;
        StatisticRepository.fetchData()
            .then(onDataReceived, onError)
            .finally(onFinally);
    })

    .controller('BarController', function ($scope, StatisticRepository) {
        $scope.statistic = [];
        $scope.weeks = [];
        $scope.regions = [];
        $scope.loading = false;
        $scope.options = {
            legend: {
                display: true
            }
        };
        
        var onDataReceived = function (data) {
            $scope.regions = Object.keys(data.statistic);
            $scope.regions = $scope.regions.slice(0,4);
            data.weeks = data.weeks.slice(0,20);
            $scope.weeks = data.weeks.map(function (element) {
                return "Week #" + element;
            });
            
            $scope.statistic = [];
            for (var region in $scope.regions) {
                var element = data.statistic[$scope.regions[region]];
                var mappedData = [];
                for (var week in data.weeks) {
                    if (element[data.weeks[week]]) {
                        mappedData.push(element[data.weeks[week]]);
                    } else {
                        mappedData.push(0);
                    }
                    
                }
                $scope.statistic.push(mappedData);
            }
        };
        
        var onError = function (error) {
            alert('We had an issue loading the data. Please reload page');
        };
        
        var onFinally = function () {
            $scope.loading = false;
        }
        
        $scope.updateStatistic = function () {
            $scope.loading = true;
            StatisticRepository.fetchData(this.dateStart)
                .then(onDataReceived, onError)
                .finally(onFinally);
        };
        
        $scope.loading = true;
        StatisticRepository.fetchData()
            .then(onDataReceived, onError)
            .finally(onFinally);
    });