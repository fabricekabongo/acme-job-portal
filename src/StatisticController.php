<?php

namespace Acme;
/*
 * This is the main controller. Thanks to Silex is simple and quick to write. 
 * It contains simple calls that I'll consume from the AngularJS Front end.
 */
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Handle the statistic api
 */
class StatisticController
{
    /**
     * @var simplexml_load_string
     */
    const DEFAULT_START_DATE = '2016-05-01';
    /**
     * @var simplexml_load_string
     */
    const START_DATE_QUERY_PARAMETER_NAME = 'start_date';
    
    /**
     * @var StatisticBuilder;
     */
     private $statisticBuilder;
    /**
     * 
     */
     private $twig;
    /**
     * 
     */
     private $logger;
     
    function __construct(StatisticBuilder $statisticBuilder, $twig, $logger)
    {
        $this->statisticBuilder = $statisticBuilder;
        $this->twig = $twig;
        $this->logger = $logger;
    }
    
    /**
     * Returns the data grouped by regions and week
     * 
     * @var Request $request
     *
     * @returns JsonResponse
     */
    public function jsonWeeklyDataAction(Request $request)
    {
        $startDate = $request->query->get(self::START_DATE_QUERY_PARAMETER_NAME, self::DEFAULT_START_DATE);
        
        try {
            $startDate = new \DateTime($startDate);
        } catch (\Exception $ex) {
            $this->logger->warning("Failed to create a DateTime object with date from user", array(
                'exception' => $ex,
                'userdata' => array(
                    'datetime' => $startDate
                )
            ));
            
            return new JsonResponse(array(
                'status' => 'error',
                'message' => 'Date format incorrect. expected Y-m-d'
            ), JsonResponse::HTTP_BAD_REQUEST);
        }
        
        try {
            $data = $this->statisticBuilder->buildStatistics($startDate);
        
            return new JsonResponse(array(
                'status' => 'success',
                'data' => $data
            ));
        } catch (\Exception $ex) {
            $this->logger->warning("Failed to build the stat in json", array(
                'exception' => $ex,
                'userdata' => array(
                    'datetime' => $startDate
                )
            ));
            return new JsonResponse(array(
                'status' => 'error',
                'message' => 'An error occured while trying we were loading the data'
            ), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    
    /**
     * Returns the data grouped by regions and week
     * 
     * @var Request $request
     *
     * @returns JsonResponse
     */
    public function csvWeeklyDataAction(Request $request)
    {
        $startDate = $request->query->get(self::START_DATE_QUERY_PARAMETER_NAME, self::DEFAULT_START_DATE);
        
        try {
            $startDate = new \DateTime($startDate);
        } catch (\Exception $ex) {
            $this->logger->warning("Failed to create a DateTime object with date from user", array(
                'exception' => $ex,
                'userdata' => array(
                    'datetime' => $startDate
                )
            ));
            
            return new Response('Date format incorrect. expected Y-m-d', JsonResponse::HTTP_BAD_REQUEST);
        }
        
        try {
            $data = $this->statisticBuilder->buildStatistics($startDate);
            $regions = array_keys($data['statistic']);
            $regions = array_merge(['Weeks'], $regions);
        
            $content = $this->twig->render('data.csv.twig', array('header' => implode(',', $regions), 'weeks' => $data['weeks'], 'statistic' => $data['statistic']));
            
            return new Response($content, Response::HTTP_OK, array(
                "Content-type" => "text/csv",
                "Content-Disposition" => "attachment; filename=file.csv",
                "Pragma: no-cache",
                "Expires: 0"
                )
            );
        } catch (\Exception $ex) {
            $this->logger->warning("Failed to build the CSV file", array(
                'exception' => $ex,
                'userdata' => array(
                    'datetime' => $startDate
                )
            ));
            return new Response('An error occured while trying we were loading the data', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
?>