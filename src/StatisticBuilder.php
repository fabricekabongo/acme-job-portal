<?php
namespace Acme;

/**
 * Build the statistics for vacancies
 */
class StatisticBuilder
{
    /**
     * @var SoapClient
     */
    private $soapClient;
    /**
     * @var int
     */
    private $sessionId;
    
    /**
     * it'll be used to filter in these regions just to make the frond end readable
     * the version without it is still available in the other branch
     * @var array
     */
    private $includedRegions = array(
        'Gauteng',
        'Freestate',
        'Limpopo',
        'Eastern Cape',
        'Western Cape',
        'KwaZulu Natal',
        'Mpumalanga	',
        'North West',
        'Northern Cape'
    );
    
    /**
     * @param SoapClient $soapClient
     * @param int $sessionId
     */
    function __construct(\SoapClient $soapClient, $sessionId)
    {
        $this->soapClient = $soapClient;
        $this->sessionId = $sessionId;
    }
    
    /**
     * Returns the statistics grouped by Region and weeks
     * of the vacancies from the date passed to now
     *
     * @param \DateTime $startDate
     * 
     * @returns array
     */
    public function buildStatistics(\DateTime $startDate)
    {
        $entriesCount = $this->getEntriesCount($startDate);
        $vacancies = $this->soapClient
            ->getAdverts($this->sessionId, $this->getFilters($startDate, $entriesCount));
        
        return $this->formatStatistics($vacancies);
    }
    
    public function formatStatistics(array $vacancies)
    {
        $regions = [];
        $weeks = [];
        
        foreach ($vacancies as $vacancy) {
            if (!in_array($vacancy->region, $this->includedRegions)) {
                continue;
            }
            
            $date = new \DateTime($vacancy->start_date);
            
            if (!array_key_exists($vacancy->region, $regions)) {
                $regions[$vacancy->region] = [];
            }
            
            if (!array_key_exists($date->format("W"), $regions[$vacancy->region])) {
                $regions[$vacancy->region][intval($date->format("W"))] = 0;
            }
            
            $regions[$vacancy->region][intval($date->format("W"))] += 1;
            
            if (!in_array($date->format("W"), $weeks)) {
                $weeks[] = intval($date->format("W"));
            }
        }
        
        sort($weeks);
        
        return [
            "statistic" => $regions,
            "weeks" => $weeks
        ];
    }
    
    /**
     * Returns the number of vacancies from the date passed to now
     *
     * @param \DateTime $startDate
     *
     * @returns int 
     */
    public function getEntriesCount(\DateTime $startDate)
    {        
        $this->soapClient->getAdverts($this->sessionId, $this->getFilters($startDate));
        $pagingData = $this->soapClient->getAdvertPagingData($this->sessionId, $this->getFilters($startDate));
        
        if (is_null($pagingData)) {
            throw new \Exception("The paging data is null");
        }
        
        return $pagingData->entries;
    }
    
    /**
     * Return the SOAP filters 
     *
     * @param int $entriesNumber
     * @param \DateTime $dateTime
     * @author Fabrice Kabongo
     * 
     * @returns array 
     */
    public function getFilters(\DateTime $dateTime, $entriesNumber = 1)
    {
        $filter = array(
            array(
                'field' => 'last_modified_date',
                'value' => $dateTime->format('Y-m-d'),
                'operator' => '>='
            ),
            array(
                'field' => 'entries_per_page',
                'value' => $entriesNumber,
                'operator' => 'exact'
            )
        );
        
        return $filter;
    }
}


?>