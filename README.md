# Acme Job Portal

This is the solution of the assessment assigned to me by Parallel Software.

Demo: [http://jobportal.fabricekabongo.tech/](http://jobportal.fabricekabongo.tech/)

I developed two versions of this solution, one with a big data set (everything your API could return), and another one where I only filter in certain regions. The idea is to make the graph and the table more readable.

* The version with less data is in the [master](https://bitbucket.org/fabricekabongo/acme-job-portal/src) branch
* The other one is on the "[bigdataset](https://bitbucket.org/fabricekabongo/acme-job-portal/src/9f86172d66c6014383b7b18b5ca5ddd451a47d0f/?at=bigdataset)" branch

The master has some features and improvement that the "bigdataset" branch doesn't have.

## Installation

You will need [PHP 5.5](http://php.net/downloads.php), [Composer](https://getcomposer.org/), [Git](https://git-scm.com/) and [Apache2 (httpd)](https://httpd.apache.org/) to be installed on your computer before continuing with this installation.

1. Clone the project on your computer `git clone git@bitbucket.org:fabricekabongo/acme-job-portal.git jobportal`

2. Install the  dependencies `composer install`

### Configuration of a VHOST on Apache2 (Optional)

* create the file `jobportal.conf` in `/etc/apache2/sites-available` folder
* paste this content in the file
```
    <VirtualHost *:80>
        ServerName jobportal.dev

        DocumentRoot /var/www/jobportal/web
        <Directory /var/www/jobportal/web>
            Options Indexes FollowSymLinks MultiViews
            AllowOverride All
            Order allow,deny
            allow from all
            <IfModule mod_rewrite.c>
                RewriteEngine On
                RewriteCond %{REQUEST_FILENAME} !-f
                RewriteRule ^(.*)$ /index.php [QSA,L]
            </IfModule>
        </Directory>
    </VirtualHost>
```
* run `a2ensite jobportal` and then `service apache2 reload`
* open `jobportal.dev` in your browser, if it doesn't load try to add jobportal.dev and localhost to your host file ( /etc/host )

### Permission

* The whole folder should be readable and executable by Apache.
* `app/cache` and `app/logs` should be writable by everyone (aka 777)

## Configuration of the application

The system relies on this file for all the configuration: `app/config.json`
```json
{
    "soap": {
        "username": "username",
        "password": "password",
        "wsdl": "https://www.acme.co.za/ws/wsdl",
        "exceptions": true,
        "trace": 1
    },
    "application": {
        "debug": true
    }
}
```
To start using the system,

* copy `app/config.json.dist` to `app/config.json`
* specify the path of the WSDL, the username and the password
* (optional) set debug to false, to start using template caching and hide errors (they are logged, you are not going loose them).

PS: logs are saved in this folder `app/logs`. the file name follows this format: log-year-month-day.log


## License

This project is under the MIT license.